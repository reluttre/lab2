"""
@file blink.py
@brief A program to make the Nucleo-64 LED blink in different patterns
@details Toggles through square wave, sinewave, and triangle wave patterns on button press

@author Robert Luttrell
@date 01/27/2021
"""
import utime
import pyb
import micropython
import math

def printWelcome():
    """
    @brief Prints a welcome message
    """
    print("Welcome to the LED blinker.")
    print("Press the blue button to cycle through LED modes.")

stateMessages = [None, "Square wave pattern selected.", "Sinewave pattern selected.", "Triangle wave pattern selected."]

state = 0
ticker1 = utime.ticks_ms()
nextStateMessage = stateMessages[1]
brightness = 0

def onButtonPressFCN(IRQ_SRC):
    """
    @brief Buton press calback function
    @param IRQ_SRC Interrupt request source
    @details Creates a new ticks_ms object, increments state, and prints state message
    """
    global state
    global ticker1
    global stateMessages
    global brightness
    ticker1 = utime.ticks_ms()
    state = state % 3 + 1
    print(stateMessages[state])

def getSquareWaveBrightness(tickerStart, tickerCur):
    """
    @brief Calculates square wave brightness
    @param tickerstart ticks_ms object created on last state change
    @param tickerCur ticks_ms object representing time for brightness calculation
    """
    delta = utime.ticks_diff(tickerCur, tickerStart) % 1000
    if delta < 500:
        return 100
    else:
        return 0

def getSineWaveBrightness(tickerStart, tickerCur):
    """
    @brief Calculates sinewave brightness
    @param tickerstart ticks_ms object created on last state change
    @param tickerCur ticks_ms object representing time for brightness calculation
    """
    delta = utime.ticks_diff(tickerCur, tickerStart)
    return 100 * (0.5 + 0.5 * math.sin((2*math.pi / 10000) * delta))

def getTriangleWaveBrightness(tickerStart, tickerCur):
    """ 
    @brief Calculates triangle wave brightness
    @param tickerstart ticks_ms object created on last state change
    @param tickerCur ticks_ms object representing time for brightness calculation
    """
    delta = utime.ticks_diff(tickerCur, tickerStart)
    return (delta % 1000) / 10

if __name__ == "__main__":
    micropython.alloc_emergency_exception_buf(100)
    hasPrintedWelcome = False
    pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
    pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
    tim2 = pyb.Timer(2, freq=20000)
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)

    while True:
        try:
            if state == 0:
                if not hasPrintedWelcome:
                    printWelcome()
                    hasPrintedWelcome = True

            if state == 1:
                # square wave
                brightness = getSquareWaveBrightness(ticker1, utime.ticks_ms())
                t2ch1.pulse_width_percent(brightness)

            if state == 2:
                # sine wave
                brightness = getSineWaveBrightness(ticker1, utime.ticks_ms())
                t2ch1.pulse_width_percent(brightness)

            if state == 3:
                # triangle wave
                brightness = getTriangleWaveBrightness(ticker1, utime.ticks_ms())
                t2ch1.pulse_width_percent(brightness)

        except KeyboardInterrupt:
            break
